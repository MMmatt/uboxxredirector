class VideosController < ApplicationController
  def show
    str = request.env['HTTP_USER_AGENT']
    str = str.split("(")[1].split(")")[0].split(";")[0]#should return iPad, iPod or iPhone if on iDevice
    if str == "iPod" or str == "iPad" or str == "iPhone" 
      @testData = "uboxx://video=#{params[:id]}"
      redirect_to @testData
    else
      @testData = "Please watch out for U-Boxx coming to the App Store!"
    end
  end

  def index
  end

end
